FROM node:13-alpine

ENV PORT=1337

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

ADD . /app

RUN npm run build

CMD [ "npm", "start" ]

EXPOSE 1337
